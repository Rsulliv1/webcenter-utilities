/*
This file can be used to write custom install/uninstall steps.
 */

package com.sena.ucm.sample.managemeta;

import intradoc.common.ExecutionContext;
import intradoc.common.ServiceException;
import intradoc.common.SystemUtils;
import intradoc.data.DataBinder;
import intradoc.data.DataException;
import intradoc.data.DataResultSet;
import intradoc.data.Workspace;
import intradoc.server.IdcExtendedLoader;
import intradoc.server.Service;
import intradoc.server.utils.CompInstallUtils;
import intradoc.shared.ComponentClassFactory;
import intradoc.shared.FilterImplementor;
import intradoc.shared.MetaFieldUtils;
import intradoc.shared.SharedObjects;

import java.util.Properties;

public class TemplateComponentInstallFilter implements FilterImplementor
{
  public int doFilter(Workspace ws, DataBinder binder, ExecutionContext cxt)
      throws DataException, ServiceException
  {
    // CS version must be greater than 7.0 to run this install filter.
    if (getSignificantVersion() <= 6)
    {
      return CONTINUE;
    }
    String param = null;
    Object paramObj = cxt.getCachedObject("filterParameter");
    if (paramObj == null || (paramObj instanceof String) == false)
    {
      return CONTINUE;
    }
    param = (String) paramObj;

    Service s = null;
    IdcExtendedLoader loader = null;

    if (cxt instanceof IdcExtendedLoader)
    {
      loader = (IdcExtendedLoader) cxt;
      if (ws == null)
      {
        ws = loader.getLoaderWorkspace();
      }
    } else if (cxt instanceof Service)
    {
      s = (Service) cxt;
      loader = (IdcExtendedLoader) ComponentClassFactory.createClassInstance(
          "IdcExtendedLoader", "intradoc.server.IdcExtendedLoader",
          "!csCustomInitializerConstructionError");
    }

    // Called after environment data has been loaded and directory locations
    // have been determined but before database has been accessed.
    if (param.equals("extraAfterConfigInit"))
    {

    }
    // Called after initial connection to database and queries have been
    // loaded but before database is used to load data into application.
    // This is a good service for performing database table manipulation.
    else if (param.equals("extraBeforeCacheLoadInit"))
    {
    }
    // Called after the last standard activity of a
    // server side application initialization. This is a good place
    // to manipulate cached data or override standard configuration
    // data.
    else if (param.equals("extraAfterServicesLoadInit"))
    {
      SystemUtils.trace("senasamplemeta", "\n\n\nTemplateComponent: Found install filter\n\n\n");
      processMeta(loader, ws, binder, cxt, true);
    }
    // Called after loading cached tables.
    else if (param.equals("initSubjects"))
    {
    }
    // Called for custom uninstallation steps.
    // NOTE: Change the uninstall filter name to have your component name
    // prefix.
    // For example: MyTestCompnentUninstallFilter
    else if (param.equals("TemplateComponentComponentUninstallFilter"))
    {
      SystemUtils.trace("senasamplemeta", "\n\n\nTemplateComponent: Found uninstall filter\n\n\n");
      processMeta(loader, ws, binder, cxt, false);
    }
    SystemUtils.trace("senasamplemeta", "\n\n\nComponent filter param: "+param+"\n\n\n");
    return CONTINUE;
  }

  protected int processMeta(IdcExtendedLoader idcextendedloader,
      Workspace ws, DataBinder binder,
      ExecutionContext cxt, boolean isInstall)
  {
    SystemUtils.trace("senasamplemeta", "Starting to process custom meta..");

    try
    {
      DataResultSet dataresultset = SharedObjects
          .getTable("TemplateComponent_Metadata");
      SystemUtils.trace("senasamplemeta", "after get table");
      if (dataresultset != null)
      {
        SystemUtils.trace("senasamplemeta", "table not null");
        dataresultset.first();
        for (; dataresultset.isRowPresent(); dataresultset.next())
        {
          Properties properties = dataresultset.getCurrentRowProps();
          String metaDefName = properties.getProperty("dName");
          SystemUtils.trace("senasamplemeta", "retrieved metadata name: " + metaDefName);
          SystemUtils.trace("senasamplemeta", "hasDocMetaDef: " + MetaFieldUtils.hasDocMetaDef(metaDefName));
          if (metaDefName == null || metaDefName.length() == 0
              || ( isInstall && MetaFieldUtils.hasDocMetaDef(metaDefName))
              || ( !isInstall && !MetaFieldUtils.hasDocMetaDef(metaDefName)))
            continue;
          try
          {
            SystemUtils.trace("senasamplemeta", "is install: " + isInstall);
            if (isInstall)
              MetaFieldUtils.updateMetaDataFromProps(ws, null, properties,metaDefName, true);
            else
              MetaFieldUtils.deleteMetaData(ws, cxt, metaDefName);
            SystemUtils.trace("senasamplemeta", "after Meta call");
          } catch (ServiceException serviceexception)
          {
            SystemUtils.error(serviceexception, (new StringBuilder()).append(
                "Error:").append(metaDefName).append(
                " senasamplemeta -- metadata field was not installed.").toString());
          }
        }

      }
    } catch (Exception exception)
    {
      SystemUtils.error(exception,
          "senasamplemeta: Unable to execute install filter.");
    }
    return 0;
  }

  protected int getSignificantVersion()
  {
    String strVersion = SystemUtils.getProductVersionInfo();
    int nIndex = strVersion.indexOf(".");
    if (nIndex != -1)
    {
      strVersion = strVersion.substring(0, nIndex);
    }
    return (Integer.valueOf(strVersion).intValue());
  }
}
